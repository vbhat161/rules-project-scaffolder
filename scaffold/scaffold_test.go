package scaffold

import (
	"os"
	"path/filepath"
	"rules-project-scaffolder/build"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestDo(t *testing.T) {
	currentDir, _ := os.Getwd()
	path := filepath.Join(currentDir, "..", "testdata", "mavenproject")
	t.Run("invalid metadata: empty path", func(t *testing.T) {
		_, err := Do(Metadata{Tool: build.Maven})
		assert.ErrorIs(t, err, errMetadata)
	})
	t.Run("invalid metadata: invalid build tool", func(t *testing.T) {
		_, err := Do(Metadata{SrcPath: currentDir})
		assert.ErrorIs(t, err, errMetadata)
	})
	t.Run("scaffold project", func(t *testing.T) {
		meta := Metadata{
			SrcPath:    path,
			TargetPath: currentDir,
			Tool:       build.Maven,
		}
		bundlePath, err := Do(meta)
		assert.NoError(t, err)
		defer os.RemoveAll(bundlePath)
		assert.NotEmpty(t, bundlePath)
	})
}

func TestCollectFiles(t *testing.T) {
	currentDir, _ := os.Getwd()
	path := filepath.Join(currentDir, "..", "testdata", "mavenproject")
	meta := Metadata{
		SrcPath:    path,
		TargetPath: currentDir,
		Tool:       build.Maven,
	}
	res, err := collectFiles(meta)
	if err != nil {
		t.Error(err)
		return
	}
	assert.Len(t, res.auxFilePaths, 2, "aux files mismatch")
	assert.Len(t, res.langFilePaths, 3, "lang files mismatch")
	assert.Len(t, res.scaffoldCfgPaths, 1, "scaffold confg files mismatch")
}
