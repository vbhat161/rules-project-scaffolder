package scaffold

import (
	"rules-project-scaffolder/build"
	"rules-project-scaffolder/cmd"

	"github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"
	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/cacert"
)

func registerScaffoldCmd(c *cli.Context) error {
	// no args
	if c.Args().Present() {
		if err := cli.ShowSubcommandHelp(c); err != nil {
			logrus.Errorf("Failed to show help commands - err: %s", err)
			return cli.Exit(err.Error(), 1)
		}
		return cmd.ErrArgs
	}

	// import CA bundle
	logrus.Debugf("importing CA certificates..")
	if err := cacert.Import(c, cacert.ImportOptions{}); err != nil {
		logrus.Errorf("Failed to import CA certificates - err: %s", err)
		return cli.Exit(err.Error(), 1)
	}

	meta := Metadata{
		SrcPath:         c.String(cmd.FlagSrcDir),
		TargetPath:      c.String(cmd.FlagTargetDirPath),
		Tool:            build.Tool(c.String(cmd.FlagBuildTool)),
		TrimFNamePrefix: c.String(cmd.FlagTrimFileNamePrefix),
	}

	// scaffold
	if _, err := Do(meta); err != nil {
		return cli.Exit(err.Error(), 2)
	}
	return nil
}

// CliCommand provides cli command equivalent to scaffold operation
func CliCommand() *cli.Command {
	return &cli.Command{
		Name:   "run",
		Usage:  "Runs the scaffold operation to generate the project",
		Action: registerScaffoldCmd,
	}
}
