package scaffold

import (
	"fmt"
	"io/fs"
	"path/filepath"
	"rules-project-scaffolder/build"

	"strings"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
)

var (
	// occurs when metadata is invalid
	errMetadata = errors.New("invalid metadata")
	// scaffold config file name
	scaffoldCfgFileName = "scaffold"
	// scaffold config file supported extensions
	supportedCfgExts = []string{".yml", ".yaml"}
)

// Metadata holds the information required to scaffold a project
type Metadata struct {
	SrcPath, TargetPath string
	Tool                build.Tool
	TrimFNamePrefix     string
}

// Validate validates the given metadata
func (m Metadata) Validate() error {
	if strings.TrimSpace(m.SrcPath) == "" {
		return fmt.Errorf("%w: empty root path", errMetadata)
	}
	if m.Tool == "" {
		return fmt.Errorf("%w: invalid build tool", errMetadata)
	}
	return nil
}

func (m Metadata) String() string {
	return fmt.Sprintf(`Metadata:
		BUILD_TOOL=%s
		PROJECT_DIR=%s
		OUT_DIR=%s
		TRIM_FILE_NAME_PREFIX=%s`, m.Tool, m.SrcPath,
		m.TargetPath, m.TrimFNamePrefix)
}

// Do analyse the files from the given root path and forms a build project.
// 1. scans all the files and categorises them
// 2. scans all the dependencies in the language files
// 3. bundles into build.ProjectName folder and places it at the root path(Metadata.SrcPath)
// 4. returns the fully-qualified project file path
func Do(meta Metadata) (string, error) {

	ctxLog := logrus.WithField("action", "scaffold")
	ctxLog.Infof("scaffolding started with metadata: %s", meta)
	if err := meta.Validate(); err != nil {
		ctxLog.WithError(err).Error("metadata validation failed")
		return "", err
	}

	res, err := collectFiles(meta)
	if err != nil {
		ctxLog.Errorf("failed to collect files - err: %s", err)
		return "", errors.Wrap(err, "collect files")
	}

	cfg, err := readCfg(res.scaffoldCfgPaths)
	if err != nil {
		ctxLog.Errorf("failed to read sacffold cfg - err: %s", err)
		return "", errors.Wrap(err, "read scaffold cfg")
	}

	ctxLog.Debugf("scanning depedencies in %d language files..", len(res.langFilePaths))

	deps, err := build.ParseDeps(meta.Tool, res.langFilePaths, cfg.Dependencies)
	if err != nil {
		ctxLog.Errorf("failed to scan dependencies - err: %s", err)
		return "", errors.Wrap(err, "dependencies scan")
	}
	ctxLog.Infof("scanned %d simple dependencies defined", len(deps))
	ctxLog.Debugf("dependencies: \n %s", deps)

	opts := build.ProjStructOps{
		SrcPath:            meta.SrcPath,
		OutPath:            meta.TargetPath,
		Deps:               deps,
		LangFiles:          res.langFilePaths,
		AuxFiles:           res.auxFilePaths,
		TrimFileNamePrefix: meta.TrimFNamePrefix,
	}

	ctxLog.Debugf("generating project with opts: \n %s", opts)
	path, err := build.GenerateProject(meta.Tool, opts)
	if err != nil {
		ctxLog.Errorf("failed to generate project - err: %s", err)
		return "", errors.Wrap(err, "generate project")
	}
	ctxLog.Infof("scaffolding success! project generated at path: '%s'", path)
	return path, nil
}

type collectResult struct {
	langFilePaths, auxFilePaths, scaffoldCfgPaths []string
}

// recursively collect and categorize files from the given file path
func collectFiles(meta Metadata) (*collectResult, error) {
	ctxlog := logrus.WithField("action", "collect files")
	ctxlog.Debugf("collecting language, auxillary and config files..")
	builder, err := build.GetBuilder(meta.Tool)
	if err != nil {
		return nil, err
	}
	res := &collectResult{}
	err = filepath.Walk(meta.SrcPath,
		func(path string, info fs.FileInfo, err error) error {
			if err != nil {
				return err
			}
			if info.IsDir() {
				return nil
			}
			if ext := filepath.Ext(info.Name()); builder.Supports(ext) {
				res.langFilePaths = append(res.langFilePaths, path)
			} else if isValidCfg(info.Name()) {
				res.scaffoldCfgPaths = append(res.scaffoldCfgPaths, path)
			} else {
				res.auxFilePaths = append(res.auxFilePaths, path)
			}
			return nil
		})

	if err != nil {
		ctxlog.Errorf("failed to walk files - %s", err)
		return res, err
	}

	ctxlog.Infof("files collected - lang: %d, aux: %d, cfg: %d", len(res.langFilePaths), len(res.auxFilePaths), len(res.scaffoldCfgPaths))
	ctxlog.Debugf("lang files: \n %s \n aux files: \n %s cfg files: \n %s", res.langFilePaths, res.auxFilePaths, res.scaffoldCfgPaths)
	return res, nil
}

// checks if given file path has valid scaffold config parameters
func isValidCfg(file string) bool {
	ext := filepath.Ext(file)
	name := strings.TrimSuffix(filepath.Base(file), ext)
	if name != scaffoldCfgFileName {
		return false
	}
	for _, e := range supportedCfgExts {
		if e == ext {
			return true
		}
	}
	return false
}
