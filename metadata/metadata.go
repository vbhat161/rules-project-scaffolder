package metadata

import "fmt"

const (
	// ProjectVendor is the vendor/maintainer of the Project
	ProjectVendor = "GitLab"

	// ProjectName is the name of the Project
	ProjectName = "Rule Project Scaffolder"
)

var (
	// ProjectVersion is a placeholder value which the Dockerfile will dynamically
	// overwrite at build time with the most recent version from the CHANGELOG.md file
	ProjectVersion = "not-configured"

	// ProjectUsage provides a one line usage string for the Project
	ProjectUsage = fmt.Sprintf("%s %s Project v%s", ProjectVendor, ProjectName, ProjectVersion)
)
