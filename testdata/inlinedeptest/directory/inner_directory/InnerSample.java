// License: MIT GitLab Inc.
/*
 * Multiple dependencies with mulit-line comment
 * scaffold: dependencies=com.gitlab.test-4@1.2.3
 * scaffold: dependencies=com.gitlab.test-5@1.2.3
 */
package directory.inner_directory;

// scaffold: dependencies=com.gitlab.test-6@1.2.3
class InnerSample {

}