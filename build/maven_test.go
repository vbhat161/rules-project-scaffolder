package build

import (
	"bytes"
	"encoding/xml"
	"fmt"
	"io/fs"
	"io/ioutil"
	"math/rand"
	"os"
	"path/filepath"
	"strings"
	"testing"
	"time"

	"github.com/creekorful/mvnparser"
	"github.com/stretchr/testify/assert"
	"gopkg.in/yaml.v3"
)

func init() {
	rand.Seed(time.Now().UnixNano())
}

func TestMavenBuilder_Meta(t *testing.T) {
	mvn := mvnBuilder{}
	assert.Equal(t, Maven, mvn.Name())
	assert.Equal(t, "pom.xml", mvn.Template())
	supported := []string{".java", ".kt", ".groovy"}
	unsupported := []string{randomExt(), randomExt(), randomExt()}
	for _, lang := range supported {
		lang := lang
		t.Run("support for "+lang, func(t *testing.T) {
			if !mvn.Supports(lang) {
				t.Errorf("unsupported language")
				return
			}
		})
	}
	for _, lang := range unsupported {
		lang := lang
		t.Run("no support for "+lang, func(t *testing.T) {
			if mvn.Supports(lang) {
				t.Errorf("should not have supported but is supporting")
				return
			}
		})
	}
}

func TestMavenBuilder_GenerateManifest(t *testing.T) {
	mvn := mvnBuilder{}
	currentDir, _ := os.Getwd()

	t.Run("no dependencies", func(t *testing.T) {
		// generated POM should be same as template file
		got, err := mvn.GenerateManifest(nil)
		if err != nil {
			t.Error(err)
			return
		}
		tmplPath := filepath.Join(currentDir, "..", "template", "pom.xml")
		want, err := ioutil.ReadFile(tmplPath)
		if err != nil {
			t.Errorf("read template file: %s", err)
			return
		}
		assert.True(t, bytes.Equal(got, want))
	})

	t.Run("invalid dependency format", func(t *testing.T) {
		deps := []Dependency{
			mockDependency{
				name:    "",
				version: "1.2.3",
			}}
		if _, err := mvn.GenerateManifest(deps); err == nil {
			t.Errorf("expected invalid maven dependency format but got empty")
		}
	})

	t.Run("invalid dependency data", func(t *testing.T) {
		deps := []Dependency{
			mvnDependency{
				GroupID: "",
				Version: "1.2.3",
			}, mvnDependency{
				GroupID:    "com.gitlab",
				ArtifactID: "",
			}}
		if _, err := mvn.GenerateManifest(deps); err == nil {
			t.Errorf("expected invalid dependency information but got empty")
		}
	})

	t.Run("valid dependencies: via data structure", func(t *testing.T) {
		deps := []Dependency{
			mvnDependency{
				GroupID:    "com.gitlab",
				ArtifactID: "test",
				Version:    "1.2.3",
			}, mvnDependency{
				GroupID:    "org.gitlab",
				ArtifactID: "test",
				Version:    "RELEASE",
			}}

		// generated POM should be same as template file
		got, err := mvn.GenerateManifest(deps)
		if err != nil {
			t.Errorf("expected no error but got: %s", err)
			return
		}

		// Load project from string
		var project mvnparser.MavenProject
		if err := xml.Unmarshal(got, &project); err != nil {
			t.Fatalf("unable to unmarshal pom file, error: %s", err)
			return
		}

		assert.Equal(t, len(deps), len(project.Dependencies))

		for _, mvnDep := range project.Dependencies {
			dep := mockDependency{
				name:    mvnDep.GroupId + "." + mvnDep.ArtifactId,
				version: mvnDep.Version,
			}
			assert.Truef(t, exists(deps, dep), "dep[%s] missing in POM", dep)
		}
	})

	t.Run("valid dependencies: via scaffold config ", func(t *testing.T) {

		currentDir, _ := os.Getwd()
		mvn := mvnBuilder{}
		projSrc := filepath.Join(currentDir, "..", "testdata", "mavenproject")

		inlineFiles := []string{
			filepath.Join(projSrc, "App.java"),
			filepath.Join(projSrc, "dir1", "TestDir1.java"), // 1 dep
			filepath.Join(projSrc, "dir2", "Dir2.java"),     // 2 dep(1 duplicate)
		}

		rawDeps, err := readDepsFromCfg(filepath.Join(projSrc, "scaffold.yml"))
		if err != nil {
			t.Error(err)
			return
		}

		mvnDeps, err := mvn.parseDeps(parseDepsOpts{
			InlineFiles: inlineFiles,
			RawDeps:     rawDeps,
		})
		if err != nil {
			t.Errorf("unexpected error while parsing deps - %s", err)
			return
		}

		// generated POM should be same as template file
		got, err := mvn.GenerateManifest(mvnDeps)
		if err != nil {
			t.Errorf("expected no error but got: %s", err)
			return
		}

		// Load project from string
		var project mvnparser.MavenProject
		if err := xml.Unmarshal(got, &project); err != nil {
			t.Fatalf("unable to unmarshal pom file, error: %s", err)
			return
		}

		assert.Equal(t, len(mvnDeps), len(project.Dependencies))

		for _, mvnDep := range project.Dependencies {
			dep := mockDependency{
				name:    mvnDep.GroupId + "." + mvnDep.ArtifactId,
				version: mvnDep.Version,
			}
			assert.Truef(t, exists(mvnDeps, dep), "dep[%s] missing in POM", dep)
		}
	})
}

func TestMavenBuilder_GenerateProject(t *testing.T) {
	currentDir, _ := os.Getwd()
	mvn := mvnBuilder{}

	t.Run("invalid options", func(t *testing.T) {
		variants := []struct {
			caseName string
			opts     ProjStructOps
		}{
			{"empty", ProjStructOps{}},
			{"no source path", ProjStructOps{
				SrcPath:   "",
				Deps:      nil,
				LangFiles: []string{"test"},
			}},
			{"no language files", ProjStructOps{
				SrcPath:   currentDir,
				LangFiles: nil,
			}},
		}
		for i := range variants {
			variant := variants[i]
			t.Run(variant.caseName, func(t *testing.T) {
				_, err := mvn.GenerateProject(variant.opts)
				if err == nil {
					t.Errorf("expected error but got empty")
				}
			})
		}
	})

	t.Run("valid case: no dependencies", func(t *testing.T) {
		srcPath := filepath.Join(currentDir, "..", "testdata", "mavenproject")
		langFiles, _ := getFiles(t, srcPath)
		opts := ProjStructOps{
			SrcPath:   srcPath,
			OutPath:   currentDir,
			LangFiles: langFiles,
		}
		outPath, err := mvn.GenerateProject(opts)
		if err != nil {
			t.Errorf("failed to generate project: %s", err)
			return
		}
		defer os.RemoveAll(outPath)
		verifyGeneratedProject(t, outPath, opts.LangFiles, opts.AuxFiles)
	})

	t.Run("valid case: with dependencies", func(t *testing.T) {
		srcPath := filepath.Join(currentDir, "..", "testdata", "mavenproject")
		langFiles, _ := getFiles(t, srcPath)
		deps := []Dependency{
			mvnDependency{
				GroupID:    "com.gitlab",
				ArtifactID: "test",
				Version:    "1.2.3",
			}, mvnDependency{
				GroupID:    "org.gitlab",
				ArtifactID: "test",
			}}

		opts := ProjStructOps{
			SrcPath:   srcPath,
			OutPath:   currentDir,
			Deps:      deps,
			LangFiles: langFiles,
		}
		outPath, err := mvn.GenerateProject(opts)
		if err != nil {
			t.Errorf("failed to generate project: %s", err)
			return
		}
		defer os.RemoveAll(outPath)
		verifyGeneratedProject(t, outPath, opts.LangFiles, opts.AuxFiles)
	})

	t.Run("valid case: with aux files", func(t *testing.T) {
		srcPath := filepath.Join(currentDir, "..", "testdata", "mavenproject")
		langFiles, auxFiles := getFiles(t, srcPath)
		deps := []Dependency{
			mvnDependency{
				GroupID:    "com.gitlab",
				ArtifactID: "test",
				Version:    "1.2.3",
			}, mvnDependency{
				GroupID:    "org.gitlab",
				ArtifactID: "test",
			}}

		opts := ProjStructOps{
			SrcPath:   srcPath,
			OutPath:   currentDir,
			Deps:      deps,
			LangFiles: langFiles,
			AuxFiles:  auxFiles,
		}
		outPath, err := mvn.GenerateProject(opts)
		if err != nil {
			t.Errorf("failed to generate project: %s", err)
			return
		}
		defer os.RemoveAll(outPath)
		verifyGeneratedProject(t, outPath, opts.LangFiles, opts.AuxFiles)
	})

	t.Run("valid case: with trim file name", func(t *testing.T) {
		srcPath := filepath.Join(currentDir, "..", "testdata", "mavenproject")
		langFiles, auxFiles := getFiles(t, srcPath)
		deps := []Dependency{
			mvnDependency{
				GroupID:    "com.gitlab",
				ArtifactID: "test",
				Version:    "1.2.3",
			}, mvnDependency{
				GroupID:    "org.gitlab",
				ArtifactID: "test",
			}}

		trimFileName := "Test"

		opts := ProjStructOps{
			SrcPath:            srcPath,
			OutPath:            currentDir,
			Deps:               deps,
			LangFiles:          langFiles,
			AuxFiles:           auxFiles,
			TrimFileNamePrefix: trimFileName,
		}
		outPath, err := mvn.GenerateProject(opts)
		if err != nil {
			t.Errorf("failed to generate project: %s", err)
			return
		}
		defer os.RemoveAll(outPath)

		assert.DirExists(t, outPath, "generated project directory does not exist")
		outLangFiles, outAuxFiles := getFiles(t, outPath)
		// confirm if file paths are preserved in the generated project
		outSrcPath := filepath.Join(outPath, "src", "main") // project files are placed in <project>/src/main/<...files..>
		for _, outFile := range outLangFiles {
			fpath := strings.TrimPrefix(outFile, outSrcPath)
			isNotInPath := !hasFilePath(langFiles, fpath)
			if isNotInPath {
				// simulate filepath before trimming and check if the path exists in
				// source lang file
				prevFileName := trimFileName + filepath.Base(fpath)
				prevFilePath := filepath.Join(filepath.Dir(fpath), prevFileName)
				if !hasFilePath(langFiles, prevFilePath) {
					t.Errorf("file path structure for file[%s] not retained", fpath)
					return
				}
			}
		}

		var containsBuildFile bool // to confirm if build file is present

		for _, outFile := range outAuxFiles {
			if strings.HasSuffix(outFile, mvnBuilder{}.Template()) {
				containsBuildFile = true
				continue
			}
			filePath := strings.TrimPrefix(outFile, outPath) // aux files are placed in the root level
			if !hasFilePath(auxFiles, filePath) {
				t.Errorf("file path structure for file[%s] not retained", filePath)
				return
			}
		}
		assert.True(t, containsBuildFile, "build file is missing")
	})
}

func TestMavenBuilder_ParseInlineDeps(t *testing.T) {
	currentDir, _ := os.Getwd()
	mvn := mvnBuilder{}
	projSrc := filepath.Join(currentDir, "..", "testdata", "mavenproject")
	files := []string{
		filepath.Join(projSrc, "App.java"),
		filepath.Join(projSrc, "dir1", "TestDir1.java"), // 1 dep
		filepath.Join(projSrc, "dir2", "Dir2.java"),     // 2 dep(1 duplicate)
	}
	expectedDeps := 2
	deps, err := mvn.parseInlineDeps(files)
	if err != nil {
		t.Errorf("expected no error but got: %s", err)
		return
	}
	assert.Len(t, deps, expectedDeps, "total expected deps mismatch")
	for _, d := range deps {
		assert.Truef(t, d.Valid(), "dependency invalid: %f", d.UID())
	}
}

func TestMavenBuilder_ParseRawDeps(t *testing.T) {
	mvn := mvnBuilder{}
	tests := []struct {
		usecase string
		dep     RawDependency
		valid   bool
	}{
		{
			usecase: "random info",
			dep: RawDependency{
				"random": "test",
			},
			valid: false,
		},
		{
			usecase: "missing group id",
			dep: RawDependency{
				"groupId": "org.gitlab",
			},
			valid: false,
		},
		{
			usecase: "missing artifact id",
			dep: RawDependency{
				"groupId": "org.gitlab",
			},
			valid: false,
		},
		{
			usecase: "valid case with no version",
			dep: RawDependency{

				"groupId":    "org.gitlab",
				"artifactId": "test",
			},
			valid: true,
		},
		{
			usecase: "valid case with version",
			dep: RawDependency{

				"groupId":    "org.gitlab",
				"artifactId": "test",
				"version":    "1.2.3",
			},
			valid: true,
		},
		{
			usecase: "valid case with meta attributes",
			dep: RawDependency{
				"groupId":    "org.gitlab",
				"artifactId": "test",
				"version":    "1.2.3",
				"exclusions": []map[string]interface{}{{
					"exclusion": map[string]interface{}{
						"groupId":    "org.gitlab",
						"artifactId": "test",
					},
				}},
			},
			valid: true,
		},
	}

	for _, variant := range tests {
		variant := variant
		t.Run(variant.usecase, func(t *testing.T) {
			deps, err := mvn.parseRawDeps([]RawDependency{variant.dep})
			if variant.valid {
				if err != nil {
					t.Errorf("expected no error but got - %s", err)
					return
				}
				assert.Len(t, deps, 1)
			} else {
				if err == nil {
					t.Errorf("expected invalid dependency error but got empty")
					return
				}
				assert.Len(t, deps, 0)
			}

		})
	}

}

// helpers

// mockDependency represents a library dependency
type mockDependency struct {
	name    string // name of the dependency
	version string // defaults to `latest` if empty
}

func (d mockDependency) Name() string {
	return d.name
}

func (d mockDependency) Ver() string {
	return d.version
}

func (d mockDependency) UID() string {
	return fmt.Sprintf("%s@%s", d.name, d.version)
}

// IsValid indicates if the dependency has valid format
func (d mockDependency) Valid() bool {
	return true
}

// reads raw dependencies defined in sacffold yaml config file
func readDepsFromCfg(cfgPath string) ([]RawDependency, error) {
	type scaffoldRepr struct {
		Dependencies []struct {
			Dependency RawDependency `yaml:"dependency"`
		} `yaml:"dependencies"`
	}
	repr := scaffoldRepr{}
	data, err := ioutil.ReadFile(cfgPath)
	if err != nil {
		return nil, fmt.Errorf("unexpected error while reading scaffold cfg - %s", err)
	}
	if err := yaml.Unmarshal(data, &repr); err != nil {
		return nil, fmt.Errorf("unexpected error while unmarshalling scaffold cfg - %s", err)
	}
	var rawDeps []RawDependency
	for _, d := range repr.Dependencies {
		rawDeps = append(rawDeps, d.Dependency)
	}
	return rawDeps, nil
}

func verifyGeneratedProject(t *testing.T, outPath string, langFiles, auxFiles []string) {
	assert.DirExists(t, outPath, "generated project directory does not exist")
	outLangFiles, outAuxFiles := getFiles(t, outPath)
	// confirm if file paths are preserved in the generated project
	outSrcPath := filepath.Join(outPath, "src", "main") // project files are placed in <project>/src/main/<...files..>
	for _, outFile := range outLangFiles {
		filePath := strings.TrimPrefix(outFile, outSrcPath)
		if !hasFilePath(langFiles, filePath) {
			t.Errorf("file path structure for file[%s] not retained", filePath)
			return
		}
	}

	var containsBuildFile bool // to confirm if build file is present

	for _, outFile := range outAuxFiles {
		if strings.HasSuffix(outFile, mvnBuilder{}.Template()) {
			containsBuildFile = true
			continue
		}
		filePath := strings.TrimPrefix(outFile, outPath) // aux files are placed in the root level
		if !hasFilePath(auxFiles, filePath) {
			t.Errorf("file path structure for file[%s] not retained", filePath)
			return
		}
	}
	assert.True(t, containsBuildFile, "build file is missing")
}

func getFiles(t *testing.T, fixtureSrcPath string) (langFiles, auxFiles []string) {
	mvn := mvnBuilder{}
	e := filepath.Walk(fixtureSrcPath, func(path string, info fs.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if info.IsDir() {
			return nil
		}
		if mvn.Supports(filepath.Ext(path)) {
			langFiles = append(langFiles, path)
		} else {
			auxFiles = append(auxFiles, path)
		}
		return nil
	})
	if e != nil {
		t.Errorf("failed to determine files in the test fixture: %s", e)
		return
	}
	return
}

func exists(deps []Dependency, d Dependency) bool {
	for _, el := range deps {
		if el.UID() == d.UID() {
			return true
		}
	}
	return false
}

func randomExt() string {
	chars := []byte("abcdefghijklmnopqrstuvw")

	random := ""
	for i := 0; i < 4; i++ {
		random += string(chars[rand.Intn(len(chars))])
	}
	return "." + random
}

func hasFilePath(filePaths []string, filePath string) bool {
	for _, fp := range filePaths {
		if strings.HasSuffix(fp, filePath) {
			return true
		}
	}
	return false
}
