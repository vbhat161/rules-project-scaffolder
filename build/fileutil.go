package build

import (
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"

	"github.com/sirupsen/logrus"
)

// ErrTemplateRead occurs when invalid template file is read
var ErrTemplateRead = errors.New("template file read")

// ReadTemplate reads template file
func readBuildTemplate(tmplName string) ([]byte, string, error) {

	projDir, err := filepath.Abs(".")
	if err != nil {
		return nil, "", fmt.Errorf("%w(%s): %s",
			ErrTemplateRead, tmplName, err)
	}
	pathParams := []string{projDir}

	// normalize project directory path by trimming internal calling modules
	// usually occurs in tests.
	internalCallers := []string{"build", "scaffold"}
	for _, caller := range internalCallers {
		if strings.HasSuffix(projDir, caller) {
			pathParams = append(pathParams, "..")
			break
		}
	}
	pathParams = append(pathParams, "template", tmplName)
	tmplFilePth := filepath.Join(pathParams...)
	logrus.Debugf("reading template file from: %s", tmplFilePth)
	tmplBytes, err := ioutil.ReadFile(tmplFilePth)
	if err != nil {
		logrus.Errorf("failed to read template file: %s", err)
		return nil, "", fmt.Errorf("%w - file name: %s error: %s",
			ErrTemplateRead, tmplName, err)
	}
	return tmplBytes, tmplFilePth, nil
}

type writeFileOpts struct {
	srcDirPth       string // directory of all source files
	srcFilePth      string // file path starting from source files directory
	targetDirPth    string // directory where the source file needs to be placed
	trimFileName    string // trims from file name's prefix before copying to target directory
	retainDirStruct bool   // if enabled, retains directory structure of the source file while placing it in the target directory
}

// writes a file from the source file path into target
// directory, with a provision to retain file path structure as present in the
// source file.
func writeFile(opts writeFileOpts) error {
	input, err := ioutil.ReadFile(opts.srcFilePth)
	if err != nil {
		return err
	}
	fileName := filepath.Base(opts.srcFilePth)
	if opts.trimFileName != "" {
		fileName = strings.TrimPrefix(fileName, opts.trimFileName)
	}

	// retains directory
	finalTargetPath := opts.targetDirPth
	if opts.retainDirStruct {
		srcDir := filepath.Dir(opts.srcFilePth)
		fileDir := strings.TrimPrefix(srcDir, filepath.Clean(opts.srcDirPth))
		if fileDir != "" {
			finalTargetPath = filepath.Join(opts.targetDirPth, fileDir)
			_ = os.MkdirAll(finalTargetPath, os.ModePerm)
		}
	}

	targetFilePth := filepath.Join(finalTargetPath, fileName)
	if err = ioutil.WriteFile(targetFilePth, input, 0644); err != nil {
		return err
	}
	return nil
}
