package build

import (
	"fmt"
)

// Dependency represents a dependency that builder can inject into it's build
// file. Since each builder has a different schema for representing a
// dependency, we can generalize upto a certain extent like this interface.
type Dependency interface {
	// Name fully qualified name of the dependency. ex: groupId + artifactId
	Name() string
	// Version name of the version
	Ver() string
	// UID is the unique ID of the depdendency. This is used to identify
	// uniqueness among the dependencies
	UID() string
	// Valid tells if given dependency is in correct format
	Valid() bool
}

type BuilderInfoProvider interface {
	// Name is the name of the build tool
	Name() Tool

	// Template is the template file name that is used for generating build file
	Template() string

	// Extensions contains all the file extensions this builder supports
	Extensions() []string

	// Supports is a helper method to indicate if the given file extension is
	// supported by the builder
	Supports(ext string) bool
}

type BuildGenerator interface {
	// parseDeps parses dependencies obtained from inline files and
	// scaffold config into builder's own dependency format
	parseDeps(opts parseDepsOpts) ([]Dependency, error)

	// GenerateManifest generates build manifest file
	// ex: pom.xml/build.gradle etc. along with injecting
	// the given dependencies. If no dependencies provided, it
	// returns default build manifest file.
	GenerateManifest(deps []Dependency) ([]byte, error)

	// GenerateProject generates the buildable project at the
	// `ProjStructOps.OutPath` location.
	GenerateProject(opts ProjStructOps) (string, error)
}

// Builder is the build tool that generates a project of its own kind.
// Ex: Maven, Gradle, SBT etc
type Builder interface {
	BuilderInfoProvider
	BuildGenerator
}

type parseDepsOpts struct {
	// InlineFiles file paths of all files which may contain inline depdendencies
	InlineFiles []string

	// RawDeps raw depdendencies that are obtained from sacffold config
	RawDeps []RawDependency
}

// ProjStructOps options provided to generate project
type ProjStructOps struct {
	SrcPath, OutPath   string
	TrimFileNamePrefix string
	Deps               []Dependency
	LangFiles          []string
	AuxFiles           []string
}

func (p *ProjStructOps) normalize() error {
	if p.SrcPath == "" {
		return fmt.Errorf("missing source path")
	}
	if p.OutPath == "" {
		p.OutPath = p.SrcPath
	}
	if len(p.LangFiles) == 0 {
		return fmt.Errorf("no language files")
	}
	for _, dep := range p.Deps {
		if !dep.Valid() {
			return fmt.Errorf("dependency[%s] is not valid", dep)
		}
	}
	return nil
}
