package build

import (
	"bytes"
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"rules-project-scaffolder/build/inlinedep"
	"strings"

	"github.com/pkg/errors"
	"gopkg.in/yaml.v3"
)

var (
	// pom xml indentation
	indentTab = "        "
	// placeholder text added in the template file which
	// is replaced by actual dependencies
	depInjectPlaceholder = "<!--INJECT_DEPENDENCIES_HERE-->"
)

// Represent a dependency of the maven project added in the scaffold config
type mvnDependency struct {
	XMLName    xml.Name `xml:"dependency" yaml:",omitempty"`
	GroupID    string   `xml:"groupId" yaml:"groupId"`
	ArtifactID string   `xml:"artifactId" yaml:"artifactId"`
	Version    string   `xml:"version" yaml:"version"`
	Classifier string   `xml:"classifier,omitempty" yaml:"classifier"`
	Type       string   `xml:"type,omitempty" yaml:"type"`
	Scope      string   `xml:"scope,omitempty" yaml:"scope"`
	Exclusions []struct {
		Exclusion mvnDepExclusion `xml:"exclusion,omitempty" yaml:"exclusion"`
	} `xml:"exclusions,omitempty" yaml:"exclusions"`
}

type mvnDepExclusion struct {
	XMLName    xml.Name `xml:"exclusion" yaml:",omitempty"`
	GroupID    string   `xml:"groupId" yaml:"groupId"`
	ArtifactID string   `xml:"artifactId" yaml:"artifactId"`
}

// Name fully qualified name of the dependency. ex: groupId + artifactId
func (d mvnDependency) Name() string {
	return fmt.Sprintf("%s.%s", d.GroupID, d.ArtifactID)
}

// Version name of the version
func (d mvnDependency) Ver() string {
	return d.Version
}

// UID is the unique ID of the depdendency. This is used to identify
// uniqueness among the dependencies
func (d mvnDependency) UID() string {
	return fmt.Sprintf("%s.%s@%s", d.GroupID, d.ArtifactID, d.Version)
}

// Valid tells if given dependency is in correct format
func (d mvnDependency) Valid() bool {
	return d.ArtifactID != "" && d.GroupID != "" && d.Version != ""
}

// maven project builder
type mvnBuilder struct{}

// Name is the name of the build tool
func (m mvnBuilder) Name() Tool {
	return Maven
}

// Template is the template file name i.e, pom.xml that is used
// for generating build file
func (m mvnBuilder) Template() string {
	return "pom.xml"
}

// Extensions contains all the file extensions that are supported by maven
func (m mvnBuilder) Extensions() []string {
	return []string{".java", ".kt", ".groovy"}
}

// Supports is a helper method to indicate if the given file extension is
// supported by maven
func (m mvnBuilder) Supports(ext string) bool {
	for _, fileExt := range m.Extensions() {
		if ext == fileExt {
			return true
		}
	}
	return false
}

// parseDeps parses dependencies obtained from inline files and
// scaffold config into maven's dependency format
func (m mvnBuilder) parseDeps(opts parseDepsOpts) ([]Dependency, error) {
	var mvnDeps []Dependency

	// inline dependencies
	inlineDeps, err := m.parseInlineDeps(opts.InlineFiles)
	if err != nil {
		return nil, errors.Wrap(err, "parse inline")
	}
	mvnDeps = append(mvnDeps, inlineDeps...)

	// raw dependencies
	rawDeps, err := m.parseRawDeps(opts.RawDeps)
	if err != nil {
		return nil, err
	}
	mvnDeps = append(mvnDeps, rawDeps...)

	return mvnDeps, nil
}

func (m mvnBuilder) parseInlineDeps(filePaths []string) ([]Dependency, error) {
	var mvnDeps []Dependency
	inlineDeps, err := inlinedep.Scan(filePaths)
	if err != nil {
		return nil, errors.Wrap(err, "parse inline")
	}
	for _, idep := range inlineDeps {
		name := idep.Name()
		idx := strings.LastIndex(name, ".")
		groupID, artifactID := name[:idx], name[idx+1:]
		mvnDeps = append(mvnDeps, mvnDependency{
			GroupID:    groupID,
			ArtifactID: artifactID,
			Version:    idep.Ver(),
		})
	}
	return mvnDeps, nil
}

func (m mvnBuilder) parseRawDeps(raw []RawDependency) ([]Dependency, error) {
	var mvnDeps []Dependency
	// rawDeps := make([]map[string]interface{}, len(raw))
	// for _, rd := range raw {
	// 	rawDeps = append(rawDeps, rd.Data)
	// }
	in, err := yaml.Marshal(raw)
	if err != nil {
		return nil, errors.Wrap(err, "parse raw deps")
	}
	var rawMvnDeps []mvnDependency
	if err := yaml.Unmarshal(in, &rawMvnDeps); err != nil {
		return nil, errors.Wrap(err, "map raw to mvn deps")
	}
	for _, mvnDep := range rawMvnDeps {
		if !mvnDep.Valid() {
			return nil, fmt.Errorf("invalid dependency - groupId: %s, artificatId: %s", mvnDep.GroupID, mvnDep.ArtifactID)
		}
		mvnDeps = append(mvnDeps, mvnDep)
	}
	return mvnDeps, nil
}

// GenerateManifest generates build manifest file
// i.e, pom.xml along with injecting
// the given dependencies. If no dependencies provided, it
// returns default build manifest file.
func (m mvnBuilder) GenerateManifest(deps []Dependency) ([]byte, error) {
	var mvnDeps []mvnDependency
	for _, dep := range deps {
		if !dep.Valid() {
			return nil, errors.New("invalid dependency")
		}
		d, ok := dep.(mvnDependency)
		if !ok {
			return nil, errors.New("invalid maven dependency format")
		}
		mvnDeps = append(mvnDeps, d)
	}
	pomFile, _, err := readBuildTemplate(m.Template())
	if err != nil {
		return nil, err
	}
	// if no deps then send the base template file as is
	if len(deps) == 0 {
		return pomFile, nil
	}
	depBytes, err := xml.MarshalIndent(mvnDeps, indentTab, indentTab)
	if err != nil {
		return nil, fmt.Errorf("marshal maven deps: %s", err)
	}
	injectedPOM := bytes.ReplaceAll(pomFile, []byte(depInjectPlaceholder), depBytes)
	return injectedPOM, nil
}

// GenerateProject generates the buildable project at the
// `ProjStructOps.OutPath` location.
func (m mvnBuilder) GenerateProject(opts ProjStructOps) (string, error) {
	if err := opts.normalize(); err != nil {
		return "", err
	}
	srcDirPath := opts.SrcPath
	targetDirPath := filepath.Join(opts.OutPath, ProjectName)

	if _, e := os.Stat(targetDirPath); e == nil {
		return "", fmt.Errorf("folder already exists at target path: '%s', delete the folder or provide different target path", targetDirPath)
	}

	err := func() error {
		// add language files inside src/main
		targetInnerDirPath := filepath.Join(targetDirPath, "src", "main")
		// create all folders
		err := os.MkdirAll(targetInnerDirPath, os.ModePerm)
		if err != nil && !os.IsExist(err) {
			return errors.Wrap(err, "create project dir")
		}
		// add languge files
		for _, filePath := range opts.LangFiles {
			retainDirStruct := true // retain dir structure for lang files
			if err := writeFile(writeFileOpts{srcDirPath, filePath, targetInnerDirPath, opts.TrimFileNamePrefix, retainDirStruct}); err != nil {
				return fmt.Errorf("add src file(%s): %w", filepath.Base(filePath), err)
			}
		}
		// add auxiliary files
		for _, filePath := range opts.AuxFiles {
			retainDirStruct := false // do not retain directory structure for aux files, place all files at root
			if err := writeFile(writeFileOpts{srcDirPath, filePath, targetDirPath, opts.TrimFileNamePrefix, retainDirStruct}); err != nil {
				return fmt.Errorf("add aux file(%s): %w", filepath.Base(filePath), err)
			}
		}
		// generate pom.xml
		pomBytes, err := m.GenerateManifest(opts.Deps)
		if err != nil {
			return err
		}
		pomFilePath := filepath.Join(targetDirPath, m.Template())
		err = ioutil.WriteFile(pomFilePath, pomBytes, 0644)
		if err != nil {
			return errors.Wrap(err, "add build file")
		}
		return nil
	}()

	if err != nil {
		_ = os.RemoveAll(targetDirPath) // remove project folder on error
		return "", err
	}

	return targetDirPath, nil
}
