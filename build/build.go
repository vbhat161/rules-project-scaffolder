package build

import (
	"errors"
	"fmt"
)

type Tool string

var (
	// tools that are supported as of now

	// Maven represents maven project
	Maven = Tool("maven")

	// ErrToolSupport occurs when invalid tool is passed
	ErrToolSupport = errors.New("tool unsupported")

	// ProjectName name of the newly scaffolded project
	ProjectName = "scaffold-project"
)

// RawDependency is the dependency information obtained from scaffold config
type RawDependency map[string]interface{}

// GetBuilder gets the relevant Builder for the Tool
func GetBuilder(t Tool) (Builder, error) {
	switch t {
	case Maven:
		return mvnBuilder{}, nil
	default:
		return nil, ErrToolSupport
	}
}

// ParseRawDeps parses raw dependencies into build tool specific
// depenedency format
func ParseDeps(t Tool, langFiles []string, raw []RawDependency) ([]Dependency, error) {
	builder, err := GetBuilder(t)
	if err != nil {
		return nil, err
	}
	if len(langFiles) == 0 && len(raw) == 0 {
		return nil, nil
	}
	return builder.parseDeps(parseDepsOpts{
		InlineFiles: langFiles,
		RawDeps:     raw,
	})
}

// GenerateProject generates buildable project
func GenerateProject(t Tool, opts ProjStructOps) (string, error) {
	builder, err := GetBuilder(t)
	if err != nil {
		return "", err
	}
	if err := opts.normalize(); err != nil {
		return "", fmt.Errorf("project structure options: %w", err)
	}
	return builder.GenerateProject(opts)
}
