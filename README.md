# Rules Test Project Scaffolder
Rule Project Scaffolder - A command line tool that takes in rule files (both language and auxiliary) along with type of build tool to generate a 
buildable project. It's primary use case is defined [here](https://gitlab.com/gitlab-org/gitlab/-/issues/355427)

**USAGE:**
`scaffolder [global options] command [command options] [arguments...]`

On success, the project generates at `<TARGET_DIR>/scaffold-project`

**AUTHOR:**
GitLab

**COMMANDS:**
- `run`      Runs the scaffold operation to generate the project
- `help`, `h`  Shows a list of commands or help for one command

**GLOBAL OPTIONS:**
- `--BUILD_TOOL value`, `-t value`             Build tool to use for the project; supported values = `maven` [env: `BUILD_TOOL`]
- `--SRC_DIR value`, `-s value`                Directory where the source files are present. [env: `SRC_DIR`, `CI_PROJECT_DIR`]
- `--TARGET_DIR value`, `-o value`             Directory where the scaffolded project should generate. [env `TARGET_DIR`, `CI_PROJECT_DIR`]
- `--TRIM_FILE_NAME_PREFIX value`, `-p value`  Trim the prefix from the language file-names [env: `TRIM_FILE_NAME_PREFIX`]
- `--DEBUG`, `-d`                              Run in DEBUG mode (default: false) [env: `DEBUG`]

**LOCAL DOCKER ENVIRONMENT SETUP**
- Build: `docker build -t scf .`
- Run: Run the following docker to generate test project(`scaffold-project`) at the project root folder location
```
docker run --rm \
--volume "${PWD}"/testdata/mavenproject:/tmp/testdata/mavenproject \
scf /scaffolder \
--BUILD_TOOL maven \
--SRC_DIR /tmp/testdata/mavenproject \
--TARGET_DIR . \
--DEBUG \
run
```

## Contributing

Contributions are welcome, see [`CONTRIBUTING.md`](CONTRIBUTING.md) for more details.

## License

This code is distributed under the MIT Expat license, see the [LICENSE](LICENSE) file.
