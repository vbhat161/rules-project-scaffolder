package cmd

import (
	"os"
	"rules-project-scaffolder/metadata"
	"strings"

	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"
	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/logutil"
)

// ErrArgs occurs when incorrect arguments are passed
var ErrArgs = errors.New("invalid arguments")

// New creates new CLI app with default properties
func New() *cli.App {
	app := cli.NewApp()
	app.Name = metadata.ProjectName
	app.Version = metadata.ProjectVersion
	app.Authors = []*cli.Author{{Name: metadata.ProjectVendor}}
	app.Usage = metadata.ProjectUsage
	app.Flags = retrieveFlags()
	setLogLevel()
	return app
}

func setLogLevel() {
	log.SetLevel(log.InfoLevel)
	for _, arg := range os.Args {
		if strings.EqualFold(arg, "--"+flagDebug) || arg == "-d" {
			log.SetLevel(log.DebugLevel)
			break
		}
	}
}

// Execute runs the CLI app
func Execute(app *cli.App, args []string) error {
	log.SetFormatter(&logutil.Formatter{Project: metadata.ProjectName})
	log.Info(metadata.ProjectUsage)
	return app.Run(args)
}
