package cmd

import "github.com/urfave/cli/v2"

const (
	// FlagBuildTool is type of the build the project should have. Ex: Maven,
	// Gradle etc
	FlagBuildTool = "BUILD_TOOL"

	// FlagSrcDir is the source directory from where files are considered
	// to build a project. Defaults to `CI_PROJECT_DIR`.
	FlagSrcDir = "SRC_DIR"

	// FlagTargetDirPath is the target directory from where the project is
	// created. Defaults to FlagSrcDir
	FlagTargetDirPath = "TARGET_DIR"

	// FlagTrimFileNamePrefix trims each file name from the given prefix if exists
	FlagTrimFileNamePrefix = "TRIM_FILE_NAME_PREFIX"

	// CI project directory
	ciProjectDir = "CI_PROJECT_DIR"

	// boolean flag that sets the app in debug mode
	flagDebug = "DEBUG"
)

// retrieveFlags retrieves flag from the CLI options
func retrieveFlags() []cli.Flag {
	return []cli.Flag{
		&cli.StringFlag{
			Name:     FlagBuildTool,
			Aliases:  []string{"t"},
			Usage:    "Build tool to use for the project; supported values = 'maven'",
			Required: true,
			EnvVars:  []string{FlagBuildTool},
		},
		&cli.StringFlag{
			Name:    FlagSrcDir,
			Aliases: []string{"s"},
			Usage:   "Directory where the source files are present.",
			EnvVars: []string{FlagSrcDir, ciProjectDir},
		},
		&cli.StringFlag{
			Name:    FlagTargetDirPath,
			Aliases: []string{"o"},
			Usage:   "Directory where the scaffolded project should generate.",
			EnvVars: []string{FlagTargetDirPath, ciProjectDir},
		},
		&cli.StringFlag{
			Name:    FlagTrimFileNamePrefix,
			Aliases: []string{"p"},
			Usage:   "Trim the prefix from the language file-names",
			EnvVars: []string{FlagTrimFileNamePrefix},
		},
		&cli.BoolFlag{
			Name:    flagDebug,
			Aliases: []string{"d"},
			Usage:   "Run in DEBUG mode",
			Value:   false,
			EnvVars: []string{flagDebug},
		},
	}
}
